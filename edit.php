<?php require_once('Connections/koneksi_penduduk.php'); ?>
<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "";
$MM_donotCheckaccess = "true";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && true) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "login.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($_SERVER['QUERY_STRING']) && strlen($_SERVER['QUERY_STRING']) > 0) 
  $MM_referrer .= "?" . $_SERVER['QUERY_STRING'];
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form2")) {
  $updateSQL = sprintf("UPDATE ktp SET `Nama Provinsi`=%s, `Nama Kabupaten/Kota`=%s, Nama=%s, `Tempat Lahir`=%s, `Tanggal Lahir`=%s, `Jenis Kelamin`=%s, Alamat=%s, Agama=%s, `Status Perkawinan`=%s, Pekerjaan=%s WHERE Nik=%s",
                       GetSQLValueString($_POST['Nama_Provinsi'], "text"),
                       GetSQLValueString($_POST['Nama_KabupatenKota'], "text"),
                       GetSQLValueString($_POST['Nama'], "text"),
                       GetSQLValueString($_POST['Tempat_Lahir'], "text"),
                       GetSQLValueString($_POST['Tanggal_Lahir'], "date"),
                       GetSQLValueString($_POST['Jenis_Kelamin'], "text"),
                       GetSQLValueString($_POST['Alamat'], "text"),
                       GetSQLValueString($_POST['Agama'], "text"),
                       GetSQLValueString($_POST['Status_Perkawinan'], "text"),
                       GetSQLValueString($_POST['Pekerjaan'], "text"),
                       GetSQLValueString($_POST['Nik'], "int"));

  mysql_select_db($database_koneksi_penduduk, $koneksi_penduduk);
  $Result1 = mysql_query($updateSQL, $koneksi_penduduk) or die(mysql_error());

  $updateGoTo = "tampilktp.php";
  if (isset($_SERVER['QUERY_STRING'])) {
    $updateGoTo .= (strpos($updateGoTo, '?')) ? "&" : "?";
    $updateGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $updateGoTo));
}

$colname_edit = "-1";
if (isset($_GET['Nik'])) {
  $colname_edit = $_GET['Nik'];
}
mysql_select_db($database_koneksi_penduduk, $koneksi_penduduk);
$query_edit = sprintf("SELECT * FROM ktp WHERE Nik = %s", GetSQLValueString($colname_edit, "int"));
$edit = mysql_query($query_edit, $koneksi_penduduk) or die(mysql_error());
$row_edit = mysql_fetch_assoc($edit);
$totalRows_edit = mysql_num_rows($edit);

mysql_select_db($database_koneksi_penduduk, $koneksi_penduduk);
$query_edit = "SELECT * FROM ktp";
$edit = mysql_query($query_edit, $koneksi_penduduk) or die(mysql_error());
$row_edit = mysql_fetch_assoc($edit);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body>
<form id="form1" name="form1" method="post" action="">
  <div align="center" style="font-size: 24px">
    <p>FORM EDIT DATA
    </p>
    <p>&nbsp;</p>
  </div>
</form>
<form action="<?php echo $editFormAction; ?>" method="post" name="form2" id="form2">
  <table align="center">
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Nama Provinsi:</td>
      <td><input type="text" name="Nama_Provinsi" value="<?php echo htmlentities($row_edit['Nama Provinsi'], ENT_COMPAT, 'utf-8'); ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Nama Kabupaten/Kota:</td>
      <td><input type="text" name="Nama_KabupatenKota" value="<?php echo htmlentities($row_edit['Nama Kabupaten/Kota'], ENT_COMPAT, 'utf-8'); ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Nik:</td>
      <td><?php echo $row_edit['Nik']; ?></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Nama:</td>
      <td><input type="text" name="Nama" value="<?php echo htmlentities($row_edit['Nama'], ENT_COMPAT, 'utf-8'); ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Tempat Lahir:</td>
      <td><input type="text" name="Tempat_Lahir" value="<?php echo htmlentities($row_edit['Tempat Lahir'], ENT_COMPAT, 'utf-8'); ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Tanggal Lahir:</td>
      <td><input type="text" name="Tanggal_Lahir" value="<?php echo htmlentities($row_edit['Tanggal Lahir'], ENT_COMPAT, 'utf-8'); ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Jenis Kelamin:</td>
      <td><select name="Jenis_Kelamin">
        <option value="pria" <?php if (!(strcmp("pria", htmlentities($row_edit['Jenis Kelamin'], ENT_COMPAT, 'utf-8')))) {echo "SELECTED";} ?>>pria</option>
        <option value="wanita" <?php if (!(strcmp("wanita", htmlentities($row_edit['Jenis Kelamin'], ENT_COMPAT, 'utf-8')))) {echo "SELECTED";} ?>>wanita</option>
      </select></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Alamat:</td>
      <td><input type="text" name="Alamat" value="<?php echo htmlentities($row_edit['Alamat'], ENT_COMPAT, 'utf-8'); ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Agama:</td>
      <td><select name="Agama">
        <option value="islam" <?php if (!(strcmp("islam", htmlentities($row_edit['Agama'], ENT_COMPAT, 'utf-8')))) {echo "SELECTED";} ?>>islam</option>
        <option value="kristen" <?php if (!(strcmp("kristen", htmlentities($row_edit['Agama'], ENT_COMPAT, 'utf-8')))) {echo "SELECTED";} ?>>kristen</option>
        <option value="hindu" <?php if (!(strcmp("hindu", htmlentities($row_edit['Agama'], ENT_COMPAT, 'utf-8')))) {echo "SELECTED";} ?>>hindu</option>
        <option value="budha" <?php if (!(strcmp("budha", htmlentities($row_edit['Agama'], ENT_COMPAT, 'utf-8')))) {echo "SELECTED";} ?>>budha</option>
      </select></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Status Perkawinan:</td>
      <td><select name="Status_Perkawinan">
        <option value="kawin" <?php if (!(strcmp("kawin", htmlentities($row_edit['Status Perkawinan'], ENT_COMPAT, 'utf-8')))) {echo "SELECTED";} ?>>kawin</option>
        <option value="belum kawin" <?php if (!(strcmp("belum kawin", htmlentities($row_edit['Status Perkawinan'], ENT_COMPAT, 'utf-8')))) {echo "SELECTED";} ?>>belum kawin</option>
      </select></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">Pekerjaan:</td>
      <td><input type="text" name="Pekerjaan" value="<?php echo htmlentities($row_edit['Pekerjaan'], ENT_COMPAT, 'utf-8'); ?>" size="32" /></td>
    </tr>
    <tr valign="baseline">
      <td nowrap="nowrap" align="right">&nbsp;</td>
      <td><input type="submit" value="Edit data" /></td>
    </tr>
  </table>
  <input type="hidden" name="MM_update" value="form2" />
  <input type="hidden" name="Nik" value="<?php echo $row_edit['Nik']; ?>" />
</form>
<p>&nbsp;</p>
</body>
</html>
<?php
mysql_free_result($edit);
?>
