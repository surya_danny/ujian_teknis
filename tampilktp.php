<?php require_once('Connections/koneksi_penduduk.php'); ?>
<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "";
$MM_donotCheckaccess = "true";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && true) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "login.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($_SERVER['QUERY_STRING']) && strlen($_SERVER['QUERY_STRING']) > 0) 
  $MM_referrer .= "?" . $_SERVER['QUERY_STRING'];
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

mysql_select_db($database_koneksi_penduduk, $koneksi_penduduk);
$query_tampil = "SELECT * FROM ktp";
$tampil = mysql_query($query_tampil, $koneksi_penduduk) or die(mysql_error());
$row_tampil = mysql_fetch_assoc($tampil);
$totalRows_tampil = mysql_num_rows($tampil);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body>
<form id="form1" name="form1" method="post" action="">
  <div align="center" style="font-size: 24px">
    <p>DATA  KTP
    </p>
    <p>&nbsp;</p>
  </div>
</form>
<div align="right">
  <table width="1271" border="2">
    <tr>
      <td width="87">Nama Provinsi</td>
      <td width="144">Nama Kabupaten/Kota</td>
      <td width="78">Nik</td>
      <td width="92">Nama</td>
      <td width="80">Tempat Lahir</td>
      <td width="81">Tanggal Lahir</td>
      <td width="82">Jenis Kelamin</td>
      <td width="98">Alamat</td>
      <td width="98">Agama</td>
      <td width="111">Status Perkawinan</td>
      <td width="116">Pekerjaan</td>
      <td colspan="2"><div align="center">Action</div></td>
    </tr>
    <?php do { ?>
      <tr>
        <td><?php echo $row_tampil['Nama Provinsi']; ?></td>
        <td><?php echo $row_tampil['Nama Kabupaten/Kota']; ?></td>
        <td><?php echo $row_tampil['Nik']; ?></td>
        <td><?php echo $row_tampil['Nama']; ?></td>
        <td><?php echo $row_tampil['Tempat Lahir']; ?></td>
        <td><?php echo $row_tampil['Tanggal Lahir']; ?></td>
        <td><?php echo $row_tampil['Jenis Kelamin']; ?></td>
        <td><?php echo $row_tampil['Alamat']; ?></td>
        <td><?php echo $row_tampil['Agama']; ?></td>
        <td><?php echo $row_tampil['Status Perkawinan']; ?></td>
        <td><?php echo $row_tampil['Pekerjaan']; ?></td>
        <td width="63"><div align="center"><a href="edit.php?Nik=<?php echo $row_tampil['Nik']; ?>">EDIT</a></div></td>
        <td width="57"><div align="center"><a href="hapusktp.php?Nik=<?php echo $row_tampil['Nik']; ?>">HAPUS</a></div></td>
      </tr>
      <?php } while ($row_tampil = mysql_fetch_assoc($tampil)); ?>
  </table>
</div>
<p><a href="caridata.php">Cari Data</a></p>
<p>&nbsp;</p>
</body>
</html>
<?php
mysql_free_result($tampil);
?>
