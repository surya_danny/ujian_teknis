<?php require_once('Connections/koneksi_penduduk.php'); ?>
<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "";
$MM_donotCheckaccess = "true";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && true) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "login.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($_SERVER['QUERY_STRING']) && strlen($_SERVER['QUERY_STRING']) > 0) 
  $MM_referrer .= "?" . $_SERVER['QUERY_STRING'];
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$colname_caridata = "-1";
if (isset($_GET['cari'])) {
  $colname_caridata = $_GET['cari'];
}
mysql_select_db($database_koneksi_penduduk, $koneksi_penduduk);
$query_caridata = sprintf("SELECT * FROM ktp WHERE Nama LIKE %s ORDER BY Nama ASC", GetSQLValueString("%" . $colname_caridata . "%", "text"));
$caridata = mysql_query($query_caridata, $koneksi_penduduk) or die(mysql_error());
$row_caridata = mysql_fetch_assoc($caridata);
$totalRows_caridata = mysql_num_rows($caridata);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body>
<form id="form1" name="form1" method="get" action="">
  <p align="center">CARI DATA  </p>
  <p>Cari Data 
    <label for="cari"></label>
    <input type="text" name="cari" id="cari" />
    <input type="submit" name="tombolcari" id="tombolcari" value="Cari" />
  </p>
  <p>&nbsp;</p>
</form>
<table border="2">
  <tr>
    <td width="51">Nama Provinsi</td>
    <td width="105">Nama Kabupaten/Kota</td>
    <td width="90">Nik</td>
    <td width="104">Nama</td>
    <td width="52">Tempat Lahir</td>
    <td width="53">Tanggal Lahir</td>
    <td width="52">Jenis Kelamin</td>
    <td width="110">Alamat</td>
    <td width="110">Agama</td>
    <td width="74">Status Perkawinan</td>
    <td width="130">Pekerjaan</td>
    <td colspan="2">ACTION</td>
  </tr>
  <?php do { ?>
    <tr>
      <td><?php echo $row_caridata['Nama Provinsi']; ?></td>
      <td><?php echo $row_caridata['Nama Kabupaten/Kota']; ?></td>
      <td><?php echo $row_caridata['Nik']; ?></td>
      <td><?php echo $row_caridata['Nama']; ?></td>
      <td><?php echo $row_caridata['Tempat Lahir']; ?></td>
      <td><?php echo $row_caridata['Tanggal Lahir']; ?></td>
      <td><?php echo $row_caridata['Jenis Kelamin']; ?></td>
      <td><?php echo $row_caridata['Alamat']; ?></td>
      <td><?php echo $row_caridata['Agama']; ?></td>
      <td><?php echo $row_caridata['Status Perkawinan']; ?></td>
      <td><?php echo $row_caridata['Pekerjaan']; ?></td>
      <td width="51"><a href="edit.php?Nik=<?php echo $row_caridata['Nik']; ?>">EDIT</a></td>
      <td width="49"><a href="hapusktp.php?Nik=<?php echo $row_caridata['Nik']; ?>">HAPUS</a></td>
    </tr>
    <?php } while ($row_caridata = mysql_fetch_assoc($caridata)); ?>
</table>
<p><a href="input.php">Tambah Data</a></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
</body>
</html>
<?php
mysql_free_result($caridata);
?>
